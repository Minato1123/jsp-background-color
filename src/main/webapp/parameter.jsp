<%-- 
    Document   : random
    Created on : Sep 23, 2018, 8:44:44 PM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            body {
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
                overflow-y: hidden;
            }
            
            h1 {
                font-size: 5rem;
            }
        </style>
    </head>
    <%
        // 宣告變數在其他 JSP 區段也可以用
        String color = "#d9ddfa"; // 從 request.getParameter("...") 取得 color 參數
        // red, green, yellow, blue, purple, black
        String[] colorArray = new String[] {
            "#ffbfbf", "#bcd6be", "#faf8cf", "#d9ddfa", "#e8d3f0", "#a8a8a8"
        };
        // 抓取 user 輸入的參數
        String colorTemp = request.getParameter("color");
        // 若 user 沒輸入參數，就隨機抓顏色
        if (colorTemp != null) {
            color = colorTemp;
        } else {
            int colorIndex = (int) (Math.random()*5);
            color = colorArray[colorIndex];
        }
    %>
    <body bgcolor="<%= color %>">
        <h1>Hello World!</h1>
    </body>
</html>
