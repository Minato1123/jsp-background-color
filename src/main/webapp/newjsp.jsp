<%-- 
    Document   : newjsp
    Created on : 2022年10月25日, 下午2:20:36
    Author     : carrie
--%>

<%-- directive  --%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
        <%
            Date d = new Date();
            for (int i = 0; i < 10; i ++) {
        %>
            <tr>
                <td><%= d %></td>
            </tr>
        <%
            };
        %>
        </table>
    </body>
</html>
